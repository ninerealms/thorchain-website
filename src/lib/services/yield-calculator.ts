import axios from 'axios';
import dayjs from 'dayjs';

interface Pool {
	asset: string;
	assetPrice: string;
	assetPriceUSD: string;
}

interface PeriodInterval {
	assetDepth: string;
	runeDepth: string;
	liquidityUnits: string;
	startTime: string;
}

interface PoolHistoryDepths {
	[key: string]: {
		hourlyMonth: PeriodInterval[];
	};
}

interface PoolStore {
	pools: Pool[];
	poolHistoryDepths: PoolHistoryDepths;
}

export const getAvailablePoolStats = async (): Promise<Pool[]> => {
	try {
		const response = await axios.get('https://midgard.ninerealms.com/v2/pools');
		return response.data;
	} catch (error) {
		console.log(error);
	}
};

export const getPoolHistoryDepths = async () => {
	try {
		const response = await axios.get('https://api.ninerealms.com/thorchain/pools/history');
		return response.data;
	} catch (error) {
		console.log(error);
	}
};

export const getPoolStore = async (): Promise<PoolStore> => {
	const availablePoolStats = await getAvailablePoolStats();
	const poolHistoryDepths = await getPoolHistoryDepths();
	return {
		pools: availablePoolStats,
		poolHistoryDepths,
	};
};

const calculateKValue = (
	intervals: PeriodInterval[],
	hours: number,
): {
	start: number;
	end: number;
} => {
	const startAt = dayjs().subtract(hours, 'hours').unix();

	const assetData = intervals.filter(
		(interval: { startTime: string; liquidityUnits: string }) =>
			+interval.startTime >= startAt && +interval.liquidityUnits !== 0,
	);

	const startingAsset = assetData[0];
	const endingAsset = assetData[assetData.length - 1];

	return {
		start:
			(+startingAsset.assetDepth * +startingAsset.runeDepth) / (+startingAsset.liquidityUnits) ** 2,
		end: (+endingAsset.assetDepth * +endingAsset.runeDepth) / (+endingAsset.liquidityUnits) ** 2,
	};
};

const calculateFeeAPY = (asset: string, timeSpanInDays: number, poolsStore: PoolStore): number => {
	const poolHistoryDepths =
		poolsStore.poolHistoryDepths[asset] && poolsStore.poolHistoryDepths[asset].hourlyMonth;

	if (!poolHistoryDepths) {
		return null;
	}

	const hours = timeSpanInDays * 24;

	const kValue = calculateKValue(poolHistoryDepths, hours);

	const returnOfInvestment = Math.sqrt(kValue.end) / Math.sqrt(kValue.start) - 1;
	const annualPercentageYield = (1 + returnOfInvestment) ** ((365 * 24) / hours) - 1;

	return annualPercentageYield;
};

const calculateFeeIncome = (
	asset: string,
	timeSpanInDays: number,
	poolsStore: PoolStore,
	dateToPredict: Date,
): number => {
	const apy = calculateFeeAPY(asset, timeSpanInDays, poolsStore);
	const hours = dayjs(dateToPredict).diff(dayjs(), 'hours');
	return (1 + apy) ** (hours / (365 * 24)) - 1;
};

const calculateImpermanentLoss = (
	assetPriceUSD: number,
	runePriceUSD: number,
	priceTargetForAsset: number,
	priceTargetForRune: number,
): number => {
	const currentPriceRatio = assetPriceUSD / runePriceUSD;
	const futurePriceRatio = priceTargetForAsset / priceTargetForRune;
	const priceSwing = currentPriceRatio / futurePriceRatio;
	return (2 * Math.sqrt(priceSwing)) / (priceSwing + 1) - 1;
};

export const calculateYieldPrediction = (
	capitalInvested: number,
	dateToPredict: Date,
	asset: string,
	timeSpanInDays: number,
	priceTargetForRune: number,
	priceTargetForAsset: number,
	poolStore: PoolStore,
): {
	keepProvidingLiquidity: number;
	withdrawAndHoldRune: number;
	withdrawAndHoldAsset: number;
	withdrawAndHoldBoth: number;
} => {
	const pool = poolStore.pools.find((pool) => pool.asset === asset);

	if (!pool) {
		return {
			keepProvidingLiquidity: 0,
			withdrawAndHoldRune: 0,
			withdrawAndHoldAsset: 0,
			withdrawAndHoldBoth: 0,
		};
	}

	const assetPriceUSD = +pool.assetPriceUSD;
	const runePriceUSD = assetPriceUSD / +pool.assetPrice;

	const withdrawAndHoldRune = (capitalInvested * priceTargetForRune) / runePriceUSD;
	const withdrawAndHoldAsset = (capitalInvested * priceTargetForAsset) / assetPriceUSD;
	const withdrawAndHoldBoth = 0.5 * (withdrawAndHoldRune + withdrawAndHoldAsset);

	const feeIncome = calculateFeeIncome(pool.asset, timeSpanInDays, poolStore, dateToPredict);

	const impermanentLoss = calculateImpermanentLoss(
		assetPriceUSD,
		runePriceUSD,
		priceTargetForAsset,
		priceTargetForRune,
	);

	const totalGain = withdrawAndHoldBoth * (1 + feeIncome + impermanentLoss);

	return {
		keepProvidingLiquidity: totalGain - capitalInvested,
		withdrawAndHoldRune: withdrawAndHoldRune - capitalInvested,
		withdrawAndHoldAsset: withdrawAndHoldAsset - capitalInvested,
		withdrawAndHoldBoth: withdrawAndHoldBoth - capitalInvested,
	};
};
