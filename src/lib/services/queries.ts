import { QueryClient, useQuery } from '@sveltestack/svelte-query';

import {
	getChainMetrics,
	getRuneMetrics,
	getTotalValueLocked,
	getYieldMetrics,
	getVaultAssets,
} from '$lib/services/metrics';
import { getLPTableData } from '$lib/services/yield';
import { getRunePrice } from '$lib/services/rune';
import { getMarketData, getPoolData, getSwapTableData } from '$lib/services/swap';
import { getPoolStore } from '$lib/services/yield-calculator';

export const queryClient = new QueryClient({
	defaultOptions: {
		queries: {
			onError: (error) => {
				console.error(error);
			},
			staleTime: 30 * 60 * 1000,
			refetchOnWindowFocus: false,
		},
	},
});

export const useMetrics = () => useQuery(['metrics'], () => getChainMetrics());
export const useLPTableData = () => useQuery(['lpTableData'], () => getLPTableData());
export const useSwapTableData = () => useQuery(['swapTableData'], () => getSwapTableData());
export const useRunePrice = () => useQuery(['runePrice'], () => getRunePrice());
export const useRuneMetrics = () => useQuery(['runeMetrics'], () => getRuneMetrics());
export const useYieldMetrics = () => useQuery(['yieldMetrics'], () => getYieldMetrics());
export const useMarketData = () => useQuery(['marketData'], () => getMarketData());
export const useTotalValueLocked = () =>
	useQuery(['totalValueLocked'], () => getTotalValueLocked());
export const usePoolData = () => useQuery(['poolData'], () => getPoolData());
export const usePoolStore = () => useQuery(['poolStore'], () => getPoolStore());
export const useVaultAssets = () => useQuery(['vaultAssets'], () => getVaultAssets());
